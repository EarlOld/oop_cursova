#include "form.h"
#include "ui_form.h"
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QHBoxLayout>
#include <QLabel>
#include <QLCDNumber>
#include <QPushButton>
#include <QSpinBox>
#include <QVBoxLayout>
#include <QMessageBox>

Form::Form(QWidget *parent) : game(NULL),
    QWidget(parent),
    ui(new Ui::Form)
{
    ui->setupUi(this);

    layout = new QVBoxLayout(this);
    settingsLayout = new QHBoxLayout;

    QPushButton* newgame = new QPushButton();
    QPushButton* exitgame = new QPushButton();

    QPixmap newG(":/gelik/new.png");
    QPixmap exG(":/gelik/exit.png");
    QIcon newGIcon(newG);
    QIcon exGIcon(exG);
    newgame->setIcon(newGIcon);
    exitgame->setIcon(exGIcon);
    newgame->setIconSize(QSize(304,70));
    exitgame->setIconSize(QSize(290,70));

    connect(newgame, SIGNAL(clicked()), this, SLOT(newGame()));
    connect(exitgame, SIGNAL(clicked()), this, SLOT(exitGame()));

    settingsLayout->addWidget(newgame);
    settingsLayout->addWidget(exitgame);

    layout->setContentsMargins(2, 2, 2, 2);
    layout->addLayout(settingsLayout);
    newGame();

    setLayout(layout);
}

Form::~Form()
{
    delete ui;
}

void Form::newGame()
{
    bool imageGameFormat = false;

    QMessageBox::StandardButton reply;

    reply = QMessageBox::question(this,"New game format", "Do you want to start image games format?", QMessageBox::Yes | QMessageBox::No);

    if(reply == QMessageBox::Yes)
    {
        imageGameFormat = true;
    }

    if(game) //удаляем старое поле
    {
        layout->removeWidget(game);
        delete game;
    }

    game = new Game(/*4, */this, imageGameFormat); //рисуем новое поле
    layout->addWidget(game);

    layout->update();

    game->hide();
    game->show();

    resize(sizeHint());
}

void Form::exitGame()
{
    MainWindow *f = new MainWindow();
    f->show();
    this->close();
}
